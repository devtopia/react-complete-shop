import React, { useEffect } from "react";

import Cart from "./components/Cart/Cart";
import Layout from "./components/Layout/Layout";
import Products from "./components/Shop/Products";
import Notification from "./components/UI/Notification";
import { fetchCartData, sendCartData } from "./store/cart-actions";
import { useAppDispatch, useAppSelector } from "./store/hooks";

let isInitial = true;

function App() {
  // const dispatch = useDispatch();
  // const showCart = useSelector(
  //   (state: { ui: UIState }) => state.ui.cartIsVisible
  // );
  // const cart = useSelector((state: { cart: CartState }) => state.cart);
  // const notification = useSelector(
  //   (state: { ui: UIState }) => state.ui.notification
  // );

  // https://redux-toolkit.js.org/tutorials/typescript
  // https://dev.classmethod.jp/articles/rtk-my-memorandum/
  const dispatch = useAppDispatch();
  const showCart = useAppSelector((state) => state.ui.cartIsVisible);
  const cart = useAppSelector((state) => state.cart);
  const notification = useAppSelector((state) => state.ui.notification);

  useEffect(() => {
    dispatch(fetchCartData());
  }, []);

  useEffect(() => {
    if (isInitial) {
      isInitial = false;
      return;
    }

    if (cart.changed) {
      dispatch(sendCartData(cart));
    }
  }, [cart]);

  return (
    <>
      {notification && (
        <Notification
          status={notification.status}
          title={notification.title}
          message={notification.message}
        />
      )}
      <Layout>
        {showCart && <Cart />}
        <Products />
      </Layout>
    </>
  );
}

export default App;
