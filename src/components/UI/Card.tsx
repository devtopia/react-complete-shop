import React, { ReactNode } from "react";

import classes from "./Card.module.css";

interface ICard {
  className?: string;
  children: ReactNode;
}

const Card: React.FC<ICard> = (props) => {
  return (
    <section
      className={`${classes.card} ${props.className ? props.className : ""}`}
    >
      {props.children}
    </section>
  );
};

export default Card;
