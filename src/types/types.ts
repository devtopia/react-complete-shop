export interface ICartItem {
  id: string;
  title: string;
  quantity: number;
  totalPrice: number;
  price: number;
}

export interface IProductItem {
  product: {
    id: string;
    title: string;
    price: number;
    description: string;
  };
}
