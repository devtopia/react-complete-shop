## Overview

このサンプルはMaximilianさんのUdemyの講座に出たサンプルをTypeScriptで作成しました。
Eslintでエラーが出ないように修正したため、JavaScriptで作成された講座のサンプルとはかなり違いがあります。

## How to set up the app

.env.localファイルを用意してFirebaseのURLを記入する。

```bash
touch .env.local
vim .env.local

REACT_APP_DATA_URL = "[firebaseのurl]/cart.json"
```
注意：constant名はREACT_APP_で書くこと。

packageをインストールする。

```bash
yarn install
```

yarnがインストールされていない場合は、先にyarnをインストールする。

```bash
npm install -g yarn
```

devサーバーを起動する。

```bash
yarn start
```

ブラウザで確認する。

http://localhost:3000

![ReactCart.gif](./public/ReduxCart.gif)
